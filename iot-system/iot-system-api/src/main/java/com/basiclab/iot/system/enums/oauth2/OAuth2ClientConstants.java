package com.basiclab.iot.system.enums.oauth2;

/**
 * OAuth2.0 客户端的通用枚举
 *
 * @author DVM
 */
public interface OAuth2ClientConstants {

    String CLIENT_ID_DEFAULT = "default";

}
