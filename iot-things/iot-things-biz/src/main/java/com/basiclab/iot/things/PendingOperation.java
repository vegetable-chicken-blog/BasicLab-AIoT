
package com.basiclab.iot.things;

public interface PendingOperation {

    boolean isCanceled();

}
