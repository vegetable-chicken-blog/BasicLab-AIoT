package com.basiclab.iot.device;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目的启动类
 * <p>
 * @author DVM
 */
@SpringBootApplication
public class DeviceServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeviceServerApplication.class, args);
    }
}
