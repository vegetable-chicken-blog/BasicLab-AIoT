package com.basiclab.iot.device.common;

/**
 * device_data数据库中方法类型
 * @author Basiclab
 */
public class FunctionTypeConstant {

    /**
     * 属性
     */
    public static final String PROPERTIES = "properties";
    /**
     * 服务
     */
    public static final String SERVICE = "service";
    /**
     * 事件
     */
    public static final String EVENT = "event";
}
