package com.basiclab.iot.device.common;

/**
 * 用户常量信息
 * 
 * @author basiclab
 */
public class UserConstants
{
    /**
     * 平台默认管理员
     */
    public static final String iot = "iot";
}
