package com.basiclab.iot.device.common;

/**
 * @author Basiclab
 */
public class TdengineConstant {

    /**
     * IOT_DEVICE数据库
     */
    public static final String IOT_DEVICE = "iot_device";

    /**
     * device_data超级表
     */
    public static final String DEVICE_DATA = "device_data";

}
