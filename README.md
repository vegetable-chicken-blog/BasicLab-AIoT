# DVM-AIoT（深度视觉机器）

<div align="center">
    <img src=".image/logo.png" width="50%" height="50%" alt="BasicLabLogo">
</div>

# 基于视频、物联、AI服务一体化解决方案

#### DVM-AIoT 引领创新智慧视觉物联网云平台，无界融合物联网技术、流媒体摄像头实时传输与AI人工智能解析，开启智能监控与数据分析新纪元。

#### 我们不仅实现了设备的互联互通，更通过深度整合高清流媒体视频流与前沿AI算法，为摄像头监控增添了智能化的眼睛——精准执行人脸与行为识别、行为分析、风险人员筛查及区域入侵检测，重新定义了安防监控、智能制造及智慧城市管理的标准。

#### 这一创举不仅提升了数据处理的实时性和精确度，还极大地扩展了物联网应用的边界，让“看见”与“理解”的能力遍布每一个物联网节点，真正意义上实践了物联网+流媒体+AI的无缝融合，推动数字化转型迈向更深更广的领域。

![DVM-AIoT平台架构.jpg](.image/DVMAIoT物联网平台架构.jpg)

## 免责声明：

DVM-AIoT是一个开源学习项目，与商业行为无关。用户在使用该项目时，应遵循法律法规，不得进行非法活动。如果DVM-AIoT发现用户有违法行为，将会配合相关机关进行调查并向政府部门举报。用户因非法行为造成的任何法律责任均由用户自行承担，如因用户使用造成第三方损害的，用户应当依法予以赔偿。使用DVM-AIoT所有相关资源均由用户自行承担风险.

## 官方文档
地址：http://pro.basiclab.top:9988/

## 在线演示（由于机器资源有限，开源版不提供演示环境，请见谅）
地址：http://pro.basiclab.top:8888/
账号：admin
密码：Basiclab@456789

## 部署安装
##### 后端程序打包
```
mvn clean package -Dmaven.test.skip=true
```
##### 启动MQTT服务端
```
# 端口：8885，Topic：device/data/#
nohup java -server -Xms512m -Xmx1024m -Djava.io.tmpdir=/var/tmp -Duser.timezone=Asia/Shanghai -jar iot-things*.jar --spring.profiles.active=dev  >iot-things.log &
```
##### 后端业务部署
```
nohup java -server -Xms512m -Xmx1024m -Djava.io.tmpdir=/var/tmp -Duser.timezone=Asia/Shanghai -jar iot-device*.jar --spring.profiles.active=dev  >iot-device.log &
nohup java -server -Xms512m -Xmx1024m -Djava.io.tmpdir=/var/tmp -Duser.timezone=Asia/Shanghai -jar iot-gateway*.jar --spring.profiles.active=dev  >iot-gateway.log &
nohup java -server -Xms512m -Xmx1024m -Djava.io.tmpdir=/var/tmp -Duser.timezone=Asia/Shanghai -jar iot-infra*.jar --spring.profiles.active=dev  >iot-infra.log &
nohup java -server -Xms512m -Xmx1024m -Djava.io.tmpdir=/var/tmp -Duser.timezone=Asia/Shanghai -jar iot-system*.jar --spring.profiles.active=dev  >iot-system.log &
```
##### 前端部署
```
pnpm install
pnpm dev
```

## 应用场景

#### 跨摄像头，车辆追踪/车牌识别（可以摄像头实时检测追踪/录像回溯检测追踪）
<img src=".image/image/car.jpg" controls="" height=350 width=800> 

#### 跨摄像头，风险人员追踪（可以摄像头实时检测追踪/录像回溯检测追踪）
<img src=".image/image/fengxian.jpg" controls="" height=350 width=800>

#### 火车站、飞机站、公园门禁机/门闸机（可以摄像头实时检测追踪/录像回溯检测追踪）
<img src=".image/image/jiandie.jpg" controls="" height=400 width=1000>

#### 安防人脸实时比对（可以摄像头实时检测追踪/录像回溯检测追踪）
<img src=".image/image/face.jpg" controls="" height=450 width=800>

## 系统截图【开源版】
<div>
  <img src=".image/banner/banner1001.png" alt="图片1" width="49%" style="margin-right: 10px">
  <img src=".image/banner/banner1002.png" alt="图片1" width="49%">
</div>
<div>
  <img src=".image/banner/banner1003.png" alt="图片1" width="49%" style="margin-right: 10px">
  <img src=".image/banner/banner1004.png" alt="图片1" width="49%">
</div>
<div>
  <img src=".image/banner/banner1005.png" alt="图片1" width="49%" style="margin-right: 10px">
  <img src=".image/banner/banner1006.png" alt="图片1" width="49%">
</div>

## 系统截图【企业版】
<div>
  <img src=".image/banner/banner51.png" alt="图片1" width="49%" style="margin-right: 10px">
  <img src=".image/banner/banner52.png" alt="图片1" width="49%">
</div>
<div>
  <img src=".image/banner/banner53.png" alt="图片1" width="49%" style="margin-right: 10px;height: 350px;">
  <img src=".image/banner/banner54.png" alt="图片1" width="49%">
</div>
<div>
  <img src=".image/banner/banner55.png" alt="图片1" width="49%" style="margin-right: 10px;height: 350px;">
  <img src=".image/banner/banner56.png" alt="图片1" width="49%">
</div>
<div>
  <img src=".image/banner/banner38.png" alt="图片1" width="49%" style="margin-right: 10px">
  <img src=".image/banner/banner40.png" alt="图片1" width="49%">
</div>
<div>
  <img src=".image/banner/banner1.png" alt="图片1" width="49%" style="margin-right: 10px">
  <img src=".image/banner/banner14.png" alt="图片1" width="49%">
</div>
<div>
  <img src=".image/banner/banner21.png" alt="图片2" width="49%" style="margin-right: 10px">
  <img src=".image/banner/banner33.png" alt="图片3" width="49%">
</div>
<div>
  <img src=".image/banner/banner31.png" alt="图片2" width="49%" style="margin-right: 10px">
  <img src=".image/banner/banner8.png" alt="图片2" width="49%">
</div>
<div>
  <img src=".image/banner/banner45.png" alt="图片2" width="49%" style="margin-right: 10px">
  <img src=".image/banner/banner46.png" alt="图片2" width="49%">
</div>
<div>
  <img src=".image/banner/banner47.png" alt="图片2" width="49%" style="margin-right: 10px">
  <img src=".image/banner/banner48.png" alt="图片2" width="49%">
</div>
<div>
  <img src=".image/banner/banner34.png" alt="图片3" width="49%" style="margin-right: 10px">
  <img src=".image/banner/banner3.png" alt="图片3" width="49%">
</div>
<div>
  <img src=".image/banner/banner3.png" alt="图片1" width="49%" style="margin-right: 10px">
  <img src=".image/banner/banner35.png" alt="图片2" width="49%">
</div>
<div>
  <img src=".image/banner/banner41.png" alt="图片1" width="49%" style="margin-right: 10px">
  <img src=".image/banner/banner42.png" alt="图片1" width="49%">
</div>

## 联系方式

<div>
  <img src=".image/商业合作.jpg" alt="商业合作" width="49%">
</div>

## 代码仓库
- Gitee仓库：https://gitee.com/DEEP-VISION-MACHINE/DVM-AIoT

## 开源协议
[MIT LICENSE](LICENSE)

## 项目外包
我们也是接外包的，如果你有项目想要外包，可以微信联系【BasicLab2024】。

团队包含专业的项目经理、架构师、前端工程师、后端工程师、测试工程师、运维工程师，可以提供全流程的外包服务。

## 致谢
感谢作者[芋道源码](https://gitee.com/zhijiantianya/yudao-cloud)提供这么棒的开源快速开发框架, 开源了好用的后台管理系统。<br>
感谢各位大佬的赞助以及对项目的指正与帮助。包括但不限于代码贡献、问题反馈、资金捐赠等各种方式的支持！以下排名不分先后：

## 版权使用说明
DVM-AIoT开源平台遵循 [MIT LICENSE](LICENSE) 协议。 允许商业使用，但务必保留类作者、Copyright 信息。